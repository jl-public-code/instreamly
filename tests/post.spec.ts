import 'jest';
import * as request from 'supertest';
import { app } from '../src/index';

describe('Jest Tests', () => {
  test('Verify Tests Work', () => {
    expect(true).toBeTruthy();
  })
})

describe('GET /api/', () => {
  it('/post/10 respond with and json', (done) => {
    request(app)
      .get('/api/post/10')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, done);
  });

  it('/post/400 respond with 404 not found', (done) => {
    request(app)
      .get('/api/post/400')
      .set('Accept', 'application/json')
      .expect(404, done);
  });

  it('/post/some_string respond with 400', (done) => {
    request(app)
      .get('/api/post/some_string')
      .set('Accept', 'application/json')
      .expect(400, done);
  });
})
