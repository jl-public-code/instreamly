import * as request from 'request-promise';

import Post from '../interfaces/post';

export class PostsService {
  private apiUrl: string;

  constructor () {
    this.apiUrl = process.env.API_URL;
  }

  public postsRequest (): Post[] {
    return request(this.apiUrl).then((posts: string) => {
      return JSON.parse(posts);
    })
  }

  public async getPost (id: number): Promise<Post> {
    const posts: Post[] = await this.postsRequest()
    return posts.find((post: Post) => {
      return post.id === id;
    })
  }
}
